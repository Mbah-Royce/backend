const AuthController = require('../controller/Auth/AuthController')
const checkToken = require('../Middleware/AuthMiddleware')
const router = require('express').Router()

router.get('/:email',checkToken, AuthController.findAllEmail)

router.post('/login', AuthController.loginUser)

module.exports = router