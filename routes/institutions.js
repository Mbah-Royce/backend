const InstitutionController = require('../controller/InstitutionController')
const router = require('express').Router()
const { InstitutionCreateValidation, InstitutionUpdateValidation } = require('../RequestValidation/Institution/InstituteRequestValidation');


router.get('/', InstitutionController.findAll)

router.post('/', InstitutionCreateValidation, InstitutionController.create)

router.put('/:institutionID', InstitutionUpdateValidation, InstitutionController.update)

router.get('/:institutionID', InstitutionController.findOne)

router.post('/:institutionID', InstitutionController.remove)

module.exports = router