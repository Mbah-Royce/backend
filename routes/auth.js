const LoginController = require('../controller/userController')
const router = require('express').Router()

router.get('/', UserController.findAll)

router.post('/', UserController.create)

module.exports = router