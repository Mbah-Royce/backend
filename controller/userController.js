const {
    apiResponse,
    validate,
    SUCCESS,
    NOT_FOUND,
    UNAUTHORISED,
    VALIDATION_ERROR,
    CREATED
} = require('./helper')

const { validationResult } = require('express-validator');
const { userListingTransformer } = require('../Transformer/transformer.js')
const User = require('../models/user.js');

/**
 * List all users
 * 
 * @param {object} req 
 * @param {object} res  
 * @returns {array}
 */
function findAll(req, res) {
    const page = parseInt(req.query.page);
    const limit = 2;
    const skipIndex = (page - 1) * limit;
    User.find()
        .skip(skipIndex)
        .limit(limit)
        .then(users => {
            // var use = users[0]
            // use.push({ "test": "dfd" })
            // users[0] = use
            // let data = users.map()
            return apiResponse(res, SUCCESS, users.map(userListingTransformer), "Users Listing");
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving todos."
            });
        });
}

/**
 * Create user
 * 
 * @param {object} req 
 * @param {object} res  
 * @returns {array}
 */
function create(req, res) {
    const user = new User({
        first_name: req.body.first_name,
        second_name: req.body.second_name,
        email: req.body.email,
        password: req.body.password,
        status: req.body.status,
        gender: req.body.gender
    });

    user.save()
        .then(data => {
            res.send(data);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while creating the Todo."
            });
        });
}

module.exports = {
    findAll,
    create
};