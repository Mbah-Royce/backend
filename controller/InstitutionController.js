const {
    apiResponse,
    validate,
    SUCCESS,
    NOT_FOUND,
    UNAUTHORISED,
    VALIDATION_ERROR,
    CREATED
} = require('./helper')

/**
 * Returns all institutions
 * 
 * @param {object} req 
 * @param {object} res  
 * @returns {array}
 */
function findAll(req, res, next) {
    var respMessage = "Listing Sucessful"
    var respData = "data" //fetch data logic
    return apiResponse(res, SUCCESS, respData, respMessage)
}

/**
 * Returns created institution
 * 
 * @param {object} req 
 * @param {object} res 
 * @returns {array}
 */
function create(req, res) {
    validate(req, res)
    var respMessage = "Created Sucessful"
    var respData = "data" //fetch data logic
    return apiResponse(res, CREATED, respData, respMessage)
}

/**
 * Returns updated institution
 * 
 * @param {object} req 
 * @param {object} res 
 * @returns {array} 
 */
function update(req, res) {
    //validate request

    var respMessage = "Updated Sucessful"
    var respData = "data" //fetch data logic
    return apiResponse(res, CREATED, respData, respMessage)
}

/**
 * Display single institution
 * 
 * @param {object} req 
 * @param {object} res 
 * @returns {array} 
 */
function findOne(req, res) {
    var respMessage = "Updated Sucessful"
    var respData = "data"
    if (respData) {
        return apiResponse(res, SUCCESS, respData, respMessage)

    } else {
        var respMessage = "Resource Not Found"
        return apiResponse(res, NOT_FOUND, respData, respMessage)
    }
}

/**
 * Delete an institution
 * 
 * @param {object} req 
 * @param {object} res 
 * @returns {array} 
 */
function remove(req, res) {
    var respMessage = "Updated Sucessful"
    var respData = "data"
    if (respData) {
        return apiResponse(res, SUCCESS, respData, respMessage)

    } else {
        var respMessage = "Resource Not Found"
        return apiResponse(res, NOT_FOUND, respData, respMessage)
    }
}

module.exports = {
    findAll,
    findOne,
    create,
    update,
    remove
}