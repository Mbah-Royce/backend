const { validationResult } = require('express-validator');

const apiResponse = ((res, respStatusCode, respData, respMessage) => {
    return res.status(respStatusCode).json({ data: respData, message: respMessage })
})
const validate = ((rep, res) => {
    try {
        const errors = validationResult(rep); // Finds the validation errors in this request and wraps them in an object with handy functions

        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() });
        }

    } catch (rep) {
        return res.status(200).json({ errors: 'yes' });
    }
})
const SUCCESS = 200
const NOT_FOUND = 404
const UNAUTHORISED = 403
const VALIDATION_ERROR = 422
const CREATED = 201
const ERROR = 500
const INVALID_BODY = 401


module.exports = {
    apiResponse,
    SUCCESS,
    NOT_FOUND,
    UNAUTHORISED,
    VALIDATION_ERROR,
    CREATED,
    ERROR,
    validate
}