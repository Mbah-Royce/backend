require('dotenv').config()
const jwt = require('jsonwebtoken')
const {
    apiResponse,
    validate,
    SUCCESS,
    NOT_FOUND,
    UNAUTHORISED,
    VALIDATION_ERROR,
    CREATED
} = require('../helper')

const usersArray = [
    {
        name: 'Daniel',
        surname: 'Ndabose',
        age: 25,
        gender: 'male',
        email: 'daniel@gmail.com',
        password: '0000'
    },
    {
        name: 'Mbay',
        surname: 'Royce',
        age: 25,
        gender: 'male',
        email: 'royce@gmail.com',
        password: '0000'
    },
    {
        name: 'Test',
        surname: 'Test',
        age: 25,
        gender: 'female',
        email: 'test@gmail.com',
        password: '0000'
    },
    ,
    {
        name: 'Royce Duplicate',
        surname: 'Royce Duplicate',
        age: 25,
        gender: 'male',
        email: 'royce@gmail.com',
        password: '1111'
    }
]

/**
 * Display single institution
 * 
 * @param {object} req 
 * @param {object} res 
 * @returns {array} 
 */
 function loginUser(req, res) {
    var respMessage = {
        message: "Login Sucessful",
        token: ''
    }
    var respData = {
        email: req.body.email,
        password: req.body.password
    }
    
    const userItem = usersArray.filter(user => user.email == respData.email && user.password == respData.password);

    if (userItem.length > 0) {

        const accessToken = jwt.sign(respData,process.env.ACCESS_TOKEN_SECRET)
        respMessage.token = accessToken

        return apiResponse(res, SUCCESS, userItem, respMessage)

    } else {
         respMessage.message = "Credentials Not Found"
        return apiResponse(res, SUCCESS, userItem, respMessage)
    }
}

/**
 * Display single institution
 * 
 * @param {object} req 
 * @param {object} res 
 * @returns {array} 
 */
 function findAllEmail(req, res) {
    var respMessage = "Search one user successfully"
    var respData = req.user
    const userItem = usersArray.filter(user => user.email == req.params.email);
    if (respData) {
        return apiResponse(res, SUCCESS, userItem, respMessage)

    } else {
        var respMessage = "Resource Not Found"
        return apiResponse(res, NOT_FOUND, userItem, respMessage)
    }
}

module.exports = {
    findAllEmail,
    loginUser
}