const app = require('express')()
const bodyParser = require('body-parser')
const cors = require('cors')
const helmet = require('helmet')
const { port } = require("./config")
const mongoose = require('mongoose')

const user = require('./routes/userRoutes')
const institution = require('./routes/institutions')

const dbConfig = require('./database/config.js');

//Database Connection
mongoose.Promise = global.Promise;

// Connecting to the database
// mongoose.connect(dbConfig.url, {
//     useNewUrlParser: true
// }).then(() => {
//     console.log("Successfully connected to the database");
// }).catch(err => {
//     console.log('Could not connect to the database. Exiting now...', err);
//     process.exit();
// });


//initialising the middlewares
app.use(helmet())
app.use(cors('*'))

// parse url encoded objects- data sent through the url
app.use(bodyParser.json())


//main endpoint
app.get("/", (req, res) => {
    res.send("<center><h1>Welcome to Concour App Backend</h1></center>")
})

app.use("/user", user)

//institution routes
app.use("/institution", institution)

app.listen(port || 5000, () => {
    console.log("Server Running Successfully on Port " + port)
})

app.use((err, req, res, next) => {
    // console.log(err);
    err.statusCode = err.statusCode || 500;
    err.message = err.message || "Internal Server Error";
    res.status(err.statusCode).json({
        message: err.message,
    });
});