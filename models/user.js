const mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
    first_name: {
        type: String,
        required: true
    },
    second_name: String,
    email: String,
    password: String,
    status: String,
    gender: {
        type: String,
        enum: ['Male', 'Female', 'Other'],
        required: true
    },
}, {
    timestamps: true
});

module.exports = mongoose.model('User', UserSchema);