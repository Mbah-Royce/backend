const userListingTransformer = ((data) => {
    return {
        "first_name": data.first_name,
        "second_name": data.second_name,
        "email": data.email,
        "status": data.status,
        "gender": data.gender,
        "_links": {
            "show": {
                "method": "GET",
                "url": "http://localhost:5000/user/" + data.id
            },
            "delete": {
                "method": "DELETE",
                "url": "http://localhost:5000/user/" + data.id
            },
            "update": {
                "method": "DELETE",
                "url": "http://localhost:5000/user/" + data.id
            }
        }
    }
})
module.exports = {
    userListingTransformer
}