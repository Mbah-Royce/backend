const { check } = require('express-validator');
exports.InstitutionUpdateValidation = [

    check('email', 'Please include a valid email').isEmail().normalizeEmail({ gmail_remove_dots: true }),

    check('name', 'Password must be 6 or more characters').isLength({ min: 6 }),

    check('number', 'Password must be 6 or more characters').isLength({ min: 6 }),

    check('status', 'Password must be 6 or more characters').isLength({ min: 6 })
]

exports.InstitutionCreateValidation = [

    check('email', 'Please include a valid email').isEmail().normalizeEmail({ gmail_remove_dots: true }),

    check('name', 'Password must be 6 or more characters').isLength({ min: 6 }),

    check('number', 'Password must be 6 or more characters').isLength({ min: 6 }),

    check('status', 'Password must be 6 or more characters').isLength({ min: 6 })
]