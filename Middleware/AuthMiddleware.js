const jwt = require('jsonwebtoken')
const {
    apiResponse,
    UNAUTHORISED,
    INVALID_BODY
} = require('../controller/helper')

/**
 * Display single institution
 * 
 * @param {object} req 
 * @param {object} res 
 * @returns {array} 
 */

 function checkToken(req,res,next){
    var respMessage = "Wrong body no token found"
    const authHeader = req.headers['authorization']
    //token comes in the form Authorization: BEARER token
    //so to get the token we will the split method to get the token

    const token = authHeader && authHeader.split(' ')[1]

    //verify if user added token to his request
    if(token==null) return apiResponse(res, INVALID_BODY, [], respMessage);

    jwt.verify(token,process.env.ACCESS_TOKEN_SECRET,(err,user)=>{
        respMessage = "Forbiden tp have access to this route"
        if(err) return apiResponse(res, UNAUTHORISED, [], respMessage);

        //getting all user information that was stored as payload in during jwt signing
        req.user = user

        next()
    })
}

module.exports = checkToken